package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GodArmorHelmet implements RecipeInterface, NameSpacedKeyInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "GodHelmet_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack godArmorHelmet = new ItemStack(Material.NETHERITE_HELMET);

        godArmorHelmet.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);

        ItemMeta godArmorHelmetItemMeta = godArmorHelmet.getItemMeta();

        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_RED + "---------------");
        lore.add(ChatColor.DARK_RED + "Blood for the blood god");
        lore.add(ChatColor.DARK_RED + "---------------");

        assert godArmorHelmetItemMeta != null;

        godArmorHelmetItemMeta.setLore(lore);

        godArmorHelmetItemMeta.setDisplayName(ChatColor.DARK_RED + "God Armor Helmet");

        godArmorHelmet.setItemMeta(godArmorHelmetItemMeta);

        ShapedRecipe godArmorHelmetShapedRecipe = new ShapedRecipe(getNameSpacedKey(), godArmorHelmet);

        godArmorHelmetShapedRecipe.shape("GGG", "GHG", "GGG");

        godArmorHelmetShapedRecipe.setIngredient('G', Material.GOLD_BLOCK);
        godArmorHelmetShapedRecipe.setIngredient('H', Material.NETHERITE_HELMET);

        return godArmorHelmetShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

}
