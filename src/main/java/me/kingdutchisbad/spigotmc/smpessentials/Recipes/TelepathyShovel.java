package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class TelepathyShovel implements NameSpacedKeyInterface, RecipeInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "TelepathyShovel_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack telepathyShovelItemStack = new ItemStack(Material.NETHERITE_SHOVEL);

        ItemMeta telepathyShovelItemMeta = telepathyShovelItemStack.getItemMeta();

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.AQUA + "---------------------------------");
        lore.add(ChatColor.BLUE + "Telepathy Shovel");
        lore.add(ChatColor.AQUA + "---------------------------------");

        assert telepathyShovelItemMeta != null;
        telepathyShovelItemMeta.setLore(lore);

        telepathyShovelItemMeta.setDisplayName(ChatColor.DARK_AQUA + "Telepathy Shovel");

        telepathyShovelItemStack.setItemMeta(telepathyShovelItemMeta);

        ShapedRecipe telepathyShovelShapedRecipe = new ShapedRecipe(getNameSpacedKey(), telepathyShovelItemStack);

        telepathyShovelShapedRecipe.shape("GGG", "GSG", "GGG");
        telepathyShovelShapedRecipe.setIngredient('G', Material.GOLD_INGOT);
        telepathyShovelShapedRecipe.setIngredient('S', Material.NETHERITE_SHOVEL);

        return telepathyShovelShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
