package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GodArmorChestplate implements NameSpacedKeyInterface, RecipeInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "GodChestPlate_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack godArmorChestplate = new ItemStack(Material.NETHERITE_CHESTPLATE);

        godArmorChestplate.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);

        ItemMeta godArmorChestplateItemMeta = godArmorChestplate.getItemMeta();

        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_RED + "---------------");
        lore.add(ChatColor.DARK_RED + "Blood for the blood god");
        lore.add(ChatColor.DARK_RED + "---------------");

        assert godArmorChestplateItemMeta != null;

        godArmorChestplateItemMeta.setLore(lore);

        godArmorChestplateItemMeta.setDisplayName(ChatColor.DARK_RED + "God Armor Chestplate");

        godArmorChestplate.setItemMeta(godArmorChestplateItemMeta);

        ShapedRecipe godArmorChestplateShapedRecipe = new ShapedRecipe(getNameSpacedKey(), godArmorChestplate);

        godArmorChestplateShapedRecipe.shape("GGG", "GHG", "GGG");

        godArmorChestplateShapedRecipe.setIngredient('G', Material.GOLD_BLOCK);
        godArmorChestplateShapedRecipe.setIngredient('H', Material.NETHERITE_CHESTPLATE);

        return godArmorChestplateShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

}
