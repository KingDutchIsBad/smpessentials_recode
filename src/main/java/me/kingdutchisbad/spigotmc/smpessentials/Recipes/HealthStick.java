package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class HealthStick implements RecipeInterface, NameSpacedKeyInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "HealthStick_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack healthStickItemStack = new ItemStack(Material.STICK);

        healthStickItemStack.addUnsafeEnchantment(Enchantment.DURABILITY, 10);

        ItemMeta healthStickItemMeta = healthStickItemStack.getItemMeta();

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.YELLOW + "--------------------------------------");
        lore.add(ChatColor.GOLD + "This stick shows the enemy's health");
        lore.add(ChatColor.GOLD + "after you hit them");
        lore.add(ChatColor.YELLOW + "--------------------------------------");

        assert healthStickItemMeta != null;
        healthStickItemMeta.setLore(lore);

        healthStickItemMeta.setDisplayName(ChatColor.GOLD + "Show Health Stick");

        healthStickItemStack.setItemMeta(healthStickItemMeta);

        ShapedRecipe healthStickShapedRecipe = new ShapedRecipe(getNameSpacedKey(), healthStickItemStack);

        healthStickShapedRecipe.shape("SSS", "SSS", "SSS");

        healthStickShapedRecipe.setIngredient('S', Material.STICK);

        return healthStickShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

}
