package me.kingdutchisbad.spigotmc.smpessentials.Recipes;


import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.PersistentDataUtil;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Backpack implements RecipeInterface, NameSpacedKeyInterface
{

    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);

    @Override
    public Recipe getRecipe()
    {
        ItemStack itemStack = new ItemStack(Material.WITHER_SKELETON_SKULL);

        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta != null) itemMeta.setDisplayName(ChatColor.GOLD + "Backpack");

        ArrayList<String> lore = new ArrayList<>();

        lore.add(ChatColor.GOLD + "-----------------------");
        lore.add(ChatColor.GOLD + "Backpack, Right Click to Open");
        lore.add(ChatColor.GOLD + "-----------------------");


        assert itemMeta != null;
        PersistentDataUtil.set(itemMeta, getNameSpacedKey(), "Backpack");

        itemMeta.setLore(lore);

        itemStack.setItemMeta(itemMeta);

        ShapedRecipe shapedRecipe = new ShapedRecipe(getNameSpacedKey(), itemStack);
        shapedRecipe.shape("GGG", "GHG", "GGG");

        shapedRecipe.setIngredient('G', Material.GOLD_INGOT);

        shapedRecipe.setIngredient('H', Material.WITHER_SKELETON_SKULL);

        return shapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(plugin, "Backpack_Key");
    }

}
