package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Launcher implements RecipeInterface, NameSpacedKeyInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Launcher_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack launcherItemStack = new ItemStack(Material.FEATHER);

        ItemMeta launcherItemMeta = launcherItemStack.getItemMeta();

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.BLUE + "----------");
        lore.add(ChatColor.DARK_AQUA + "Launcher which launches you high up in the air");
        lore.add(ChatColor.BLUE + "----------");

        assert launcherItemMeta != null;
        launcherItemMeta.setLore(lore);


        launcherItemMeta.setDisplayName(ChatColor.DARK_AQUA + "Launcher");

        launcherItemStack.setItemMeta(launcherItemMeta);

        ShapedRecipe launcherShapedRecipe = new ShapedRecipe(getNameSpacedKey(), launcherItemStack);

        launcherShapedRecipe.shape("BBB", "BFB", "BBB");
        launcherShapedRecipe.setIngredient('B', Material.BLAZE_ROD);
        launcherShapedRecipe.setIngredient('F', Material.FEATHER);

        return launcherShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

}
