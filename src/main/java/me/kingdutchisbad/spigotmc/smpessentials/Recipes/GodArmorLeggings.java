package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GodArmorLeggings implements RecipeInterface, NameSpacedKeyInterface
{
    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "GodLeggings_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack godArmorLeggings = new ItemStack(Material.NETHERITE_LEGGINGS);

        godArmorLeggings.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);

        ItemMeta godArmorLeggingsItemMeta = godArmorLeggings.getItemMeta();

        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_RED + "---------------");
        lore.add(ChatColor.DARK_RED + "Blood for the blood god");
        lore.add(ChatColor.DARK_RED + "---------------");

        assert godArmorLeggingsItemMeta != null;

        godArmorLeggingsItemMeta.setLore(lore);

        godArmorLeggingsItemMeta.setDisplayName(ChatColor.DARK_RED + "God Armor Leggings");

        godArmorLeggings.setItemMeta(godArmorLeggingsItemMeta);

        ShapedRecipe godArmorLeggingsShapedRecipe = new ShapedRecipe(getNameSpacedKey(), godArmorLeggings);

        godArmorLeggingsShapedRecipe.shape("GGG", "GHG", "GGG");

        godArmorLeggingsShapedRecipe.setIngredient('G', Material.GOLD_BLOCK);
        godArmorLeggingsShapedRecipe.setIngredient('H', Material.NETHERITE_LEGGINGS);

        return godArmorLeggingsShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
