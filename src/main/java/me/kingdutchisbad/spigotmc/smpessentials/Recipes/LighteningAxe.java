package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class LighteningAxe implements RecipeInterface, NameSpacedKeyInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "LighteningAxe_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack lighteningAxeItemStack = new ItemStack(Material.NETHERITE_AXE);

        lighteningAxeItemStack.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);

        ItemMeta lighteningAxeItemMeta = lighteningAxeItemStack.getItemMeta();

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.RED + "---------------");
        lore.add("This Axe will kill anyone");
        lore.add(ChatColor.RED + "---------------");

        assert lighteningAxeItemMeta != null;
        lighteningAxeItemMeta.setLore(lore);

        lighteningAxeItemStack.setItemMeta(lighteningAxeItemMeta);

        ShapedRecipe lighteningAxeShapedRecipe = new ShapedRecipe(getNameSpacedKey(), lighteningAxeItemStack);

        lighteningAxeShapedRecipe.shape("NNN", "NIN", "NNN");

        lighteningAxeShapedRecipe.setIngredient('N', Material.NETHERITE_INGOT);
        lighteningAxeShapedRecipe.setIngredient('I', Material.IRON_AXE);

        return lighteningAxeShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
