package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GodArmorBoots implements RecipeInterface, NameSpacedKeyInterface
{
    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "GodBoots_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack godArmorBoots = new ItemStack(Material.NETHERITE_BOOTS);

        godArmorBoots.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);

        ItemMeta godArmorBootsItemMeta = godArmorBoots.getItemMeta();

        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_RED + "---------------");
        lore.add(ChatColor.DARK_RED + "Blood for the blood god");
        lore.add(ChatColor.DARK_RED + "---------------");

        assert godArmorBootsItemMeta != null;

        godArmorBootsItemMeta.setLore(lore);

        godArmorBootsItemMeta.setDisplayName(ChatColor.DARK_RED + "God Armor Boots");

        godArmorBoots.setItemMeta(godArmorBootsItemMeta);

        ShapedRecipe godArmorBootsShapedRecipe = new ShapedRecipe(getNameSpacedKey(), godArmorBoots);

        godArmorBootsShapedRecipe.shape("GGG", "GHG", "GGG");

        godArmorBootsShapedRecipe.setIngredient('G', Material.GOLD_BLOCK);
        godArmorBootsShapedRecipe.setIngredient('H', Material.NETHERITE_BOOTS);

        return godArmorBootsShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
