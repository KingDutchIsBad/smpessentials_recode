package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class PortableCraftingTable implements RecipeInterface, NameSpacedKeyInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Crafting_Key");
    }

    @Override
    public Recipe getRecipe()
    {

        ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD);

        ItemMeta itemMeta = itemStack.getItemMeta();

        assert itemMeta != null;
        itemMeta.setDisplayName(ChatColor.GREEN + "Portable Crafting Table");

        ArrayList<String> lore = new ArrayList<>();

        lore.add(ChatColor.DARK_GREEN + "Portable Crafting Table");

        itemMeta.setLore(lore);

        itemStack.setItemMeta(itemMeta);

        ShapedRecipe shapedRecipe = new ShapedRecipe(getNameSpacedKey(), itemStack);

        shapedRecipe.shape("CCC", "CCC", "CCC");

        shapedRecipe.setIngredient('C', Material.CRAFTING_TABLE);

        return shapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

}
