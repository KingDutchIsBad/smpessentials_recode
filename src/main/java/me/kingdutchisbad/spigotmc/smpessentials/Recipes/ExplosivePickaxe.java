package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ExplosivePickaxe implements RecipeInterface, NameSpacedKeyInterface
{
    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "ExplosivePickaxe_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack explosivePickaxe = new ItemStack(Material.NETHERITE_PICKAXE);

        ItemMeta explosivePickaxeItemMeta = explosivePickaxe.getItemMeta();

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.RED + "Explosive Break I");

        assert explosivePickaxeItemMeta != null;
        explosivePickaxeItemMeta.setDisplayName("Explosion Pickaxe");

        explosivePickaxeItemMeta.setLore(lore);

        explosivePickaxe.setItemMeta(explosivePickaxeItemMeta);

        ShapedRecipe explosivePickaxeShapedRecipe = new ShapedRecipe(getNameSpacedKey(), explosivePickaxe);

        explosivePickaxeShapedRecipe.shape("TTT", "TNT", "TTT");
        explosivePickaxeShapedRecipe.setIngredient('T', Material.TNT);
        explosivePickaxeShapedRecipe.setIngredient('N', Material.NETHERITE_PICKAXE);


        return explosivePickaxeShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
