package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class AncientSword implements RecipeInterface, NameSpacedKeyInterface
{
    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "AncientSword_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack ancientSwordItemStack = new ItemStack(Material.NETHERITE_SWORD);

        ItemMeta ancientSwordItemMeta = ancientSwordItemStack.getItemMeta();

        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_AQUA + "--------------------------------------------");
        lore.add(ChatColor.AQUA + "Ancient Sword");
        lore.add(ChatColor.AQUA + "This sword will make the entity you hit bleed");
        lore.add(ChatColor.DARK_AQUA + "--------------------------------------------");

        assert ancientSwordItemMeta != null;

        ancientSwordItemMeta.setLore(lore);

        ancientSwordItemMeta.setDisplayName(ChatColor.DARK_AQUA + "Ancient Sword");

        ancientSwordItemStack.setItemMeta(ancientSwordItemMeta);

        ShapedRecipe ancientSwordShapedRecipe = new ShapedRecipe(getNameSpacedKey(), ancientSwordItemStack);

        ancientSwordShapedRecipe.shape("GGG", "GHG", "GGG");

        ancientSwordShapedRecipe.setIngredient('G', Material.NETHERITE_SCRAP);
        ancientSwordShapedRecipe.setIngredient('H', Material.NETHERITE_SWORD);

        return ancientSwordShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
