package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class TeleportBow implements RecipeInterface, NameSpacedKeyInterface
{
    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "TeleportBow_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack teleportBowItemStack = new ItemStack(Material.BOW);

        teleportBowItemStack.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 2);

        ItemMeta teleportBowItemMeta = teleportBowItemStack.getItemMeta();

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.BLUE + "---------------");
        lore.add(ChatColor.BLUE + "Teleport Bow");
        lore.add(ChatColor.BLUE + "---------------");

        assert teleportBowItemMeta != null;
        teleportBowItemMeta.setLore(lore);

        teleportBowItemMeta.setDisplayName(ChatColor.DARK_BLUE + "Teleport Bow");

        ShapedRecipe teleportBowShapedRecipe = new ShapedRecipe(getNameSpacedKey(), teleportBowItemStack);

        teleportBowShapedRecipe.shape("NNN", "NBN", "NNN");
        teleportBowShapedRecipe.setIngredient('N', Material.NETHERITE_INGOT);
        teleportBowShapedRecipe.setIngredient('B', Material.BOW);

        return teleportBowShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }
}
