package me.kingdutchisbad.spigotmc.smpessentials.Recipes;

import me.kingdutchisbad.spigotmc.smpessentials.SmpEssentials;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.NameSpacedKeyInterface;
import me.kingdutchisbad.spigotmc.smpessentials.Utils.RecipeInterface;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GlowStick implements RecipeInterface, NameSpacedKeyInterface
{

    @Override
    public NamespacedKey getNameSpacedKey()
    {
        return new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "GlowStick_Key");
    }

    @Override
    public Recipe getRecipe()
    {
        ItemStack glowStickItemStack = new ItemStack(Material.STICK);

        ItemMeta glowStickItemMeta = glowStickItemStack.getItemMeta();

        assert glowStickItemMeta != null;
        glowStickItemMeta.setDisplayName(ChatColor.GOLD + "GlowStick");

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.GOLD + "Glow Effect I");

        glowStickItemMeta.setLore(lore);

        glowStickItemStack.setItemMeta(glowStickItemMeta);

        ShapedRecipe glowStickShapedRecipe = new ShapedRecipe(getNameSpacedKey(), glowStickItemStack);

        glowStickShapedRecipe.shape("   ", "RSR", "   ");

        glowStickShapedRecipe.setIngredient('R', Material.BLAZE_ROD);
        glowStickShapedRecipe.setIngredient('S', Material.STICK);

        return glowStickShapedRecipe;
    }

    @Override
    public void registerRecipe()
    {
        if (Bukkit.getRecipe(getNameSpacedKey()) == null) Bukkit.addRecipe(getRecipe());
    }

}
