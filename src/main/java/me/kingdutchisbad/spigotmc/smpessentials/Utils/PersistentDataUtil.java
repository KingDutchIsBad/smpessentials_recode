package me.kingdutchisbad.spigotmc.smpessentials.Utils;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class PersistentDataUtil
{

    private static PersistentDataContainer get(ItemMeta itemMeta)
    {
        return itemMeta.getPersistentDataContainer();
    }

    public static void set(ItemMeta itemMeta, NamespacedKey namespacedKey, String value)
    {
        get(itemMeta).set(namespacedKey, PersistentDataType.STRING, value);
    }

    public static void set(ItemMeta itemMeta, NamespacedKey namespacedKey, float value)
    {
        get(itemMeta).set(namespacedKey, PersistentDataType.FLOAT, value);
    }

    public static void set(ItemMeta itemMeta, NamespacedKey namespacedKey, double value)
    {
        get(itemMeta).set(namespacedKey, PersistentDataType.DOUBLE, value);
    }

    public static void set(ItemMeta itemMeta, NamespacedKey namespacedKey, int value)
    {
        get(itemMeta).set(namespacedKey, PersistentDataType.INTEGER, value);
    }

}
