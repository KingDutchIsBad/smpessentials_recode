package me.kingdutchisbad.spigotmc.smpessentials.Utils;


import org.bukkit.inventory.Recipe;

//Again for convince and for keeping the code clean
public interface RecipeInterface
{
    Recipe getRecipe();

    void registerRecipe();
}
