package me.kingdutchisbad.spigotmc.smpessentials.Utils;

import org.bukkit.NamespacedKey;

// Just for Convince
public interface NameSpacedKeyInterface
{
    NamespacedKey getNameSpacedKey();
}
