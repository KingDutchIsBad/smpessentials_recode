package me.kingdutchisbad.spigotmc.smpessentials;


import me.kingdutchisbad.spigotmc.smpessentials.Recipes.*;
import org.bukkit.plugin.java.JavaPlugin;

public final class SmpEssentials extends JavaPlugin
{

    @Override
    public void onEnable()
    {
        registerAllRecipes();
    }

    private void registerAllRecipes()
    {
        new Backpack().registerRecipe();
        new GodArmorHelmet().registerRecipe();
        new GodArmorChestplate().registerRecipe();
        new GodArmorLeggings().registerRecipe();
        new GodArmorBoots().registerRecipe();
        new AncientSword().registerRecipe();
        new ExplosivePickaxe().registerRecipe();
        new GlowStick().registerRecipe();
        new HealthStick().registerRecipe();
        new Launcher().registerRecipe();
        new LighteningAxe().registerRecipe();
        new PortableCraftingTable().registerRecipe();
        new TelepathyShovel().registerRecipe();
        new TeleportBow().registerRecipe();

    }
    
}
